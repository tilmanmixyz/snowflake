{
  config,
  pkgs,
  lib,
  ...
}: {
  imports = [
    ./tilman.nix
  ];
  # Nixpkgs config
  nixpkgs = {
    config.allowUnfree = true;
  };

  # Nix package manager config
  nix = {
    package = pkgs.nixUnstable;
    settings = {
      experimental-features = ["nix-command" "flakes"];
      substituters = [
        "https://helix.cachix.org"
      ];
      trusted-substituters = [
        "https://helix.cachix.org"
      ];
      trusted-public-keys = [
        "helix.cachix.org-1:ejp9KQpR1FBI2onstMQ34yogDm4OgU2ru6lIwPvuCVs="
      ];
    };
    gc = {
      automatic = true;
      options = "--delete-older-than 5d";
    };
  };

  # Programm config
  programs = {
    seahorse = {
      enable = true;
    };
    ccache = {
      enable = true;
    };
    neovim = {
      enable = true;
    };
    thefuck = {
      enable = true;
      alias = "fuck";
    };
    # ZShell
    zsh = {
      enable = true;
      shellAliases = {
        "ls" = "eza -lha --group-directories-first --icons";
        "tree" = "eza -T --icons";
        "gca" = "git add . && git commit";
        "gp" = "git push";
        "gcl" = "git clone";
        "gcm" = "git commit";
        "cat" = "bat";
        "man" = "batman";
        "grep" = "rg";
        "df" = "df -h";
        "nxu" = "nix flake update ~/snowflake && doas nixos-rebuild switch --flake ~/snowflake --show-trace";
        ":q" = "exit";
      };
      autosuggestions = {
        enable = true;
      };
      enableCompletion = true;
      enableBashCompletion = true;
      syntaxHighlighting = {
        enable = true;
      };
      # shellInit = ''
      #   autoload -U compinit
      #   zstyle ':completion:*' menu select
      #   zmodload zsh/complist
      #   compinit
      # '';
      promptInit = ''
      NEWLINE=$'\n'
      autoload -U colors && colors
      PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$NEWLINE%}%{$reset_color%}>%b "
      # PS1="%{$fg[magenta]%}%~%{$NEWLINE%}%{reset_color%}󰘧 "
      '';
    };
    # Starship Prompt
    starship = {
      enable = false;
      settings = {
        add_newline = true;
        format = lib.concatStrings [
          "$username"
          "$hostname"
          "$directory"
          "$git_branch"
          "$git_state"
          "$git_status"
          "$cmd_duration"
          "$line_break"
          "$character"
        ];
        directory = {
          style = "blue";
          truncation_length = 100;
          truncate_to_repo = false;
          format = "[$path]($style)[$read_only]($read_only_style) ";
        };
        character = {
          success_symbol = "[󰘧](green)";
          error_symbol = "[󰘧](red)";
          vimcmd_symbol = "[](purple)";
        };
        git_branch = {
          format = "[$branch]($style)";
          style = "bright-black";
        };
        git_status = {
          format = "[[(*$conflicted$untracked$modified$staged$renamed$deleted)](218) ($ahead_behind$stashed)]($style)";
          style = "cyan";
          conflicted = "​";
          untracked = "​";
          modified = "​";
          staged = "​";
          renamed = "​";
          deleted = "​";
          stashed = "≡";
        };
        git_state = {
          format = "\\([$state( $progress_current/$progress_total)]($style)\\) ";
          style = "bright-black";
        };
        cmd_duration = {
          format = " [$duration]($style) ";
          style = "yellow";
        };
      };
    };
    # ssh
    ssh = {
      askPassword = "";
    };
    gnupg = {
      agent = {
        enable = true;
        pinentryFlavor = "gnome3";
      };
    };
  };

  documentation = {
    man.generateCaches = true;
    dev.enable = true;
    doc.enable = true;
  };

  # Packages
  environment.systemPackages = with pkgs; [
    # Missing Linux Developer Manual
    man-pages

    # Printing
    system-config-printer

    # Backup
    pika-backup

    # Mail
    thunderbird

    # virtualisation
    ## KVM
    virt-manager
    ## Container
    ### Podman
    # buildah
    ### Containerd
    # nerdctl

    # DE/WM
    ## XFCE stuff
    xfce.thunar
    xfce.thunar-volman
    ## Keyring
    libsecret

    # xfce.catfish
    # xfce.gigolo
    # xfce.orage
    # xfce.xfburn
    # xfce.xfce4-appfinder
    # xfce.xfce4-clipman-plugin
    # xfce.xfce4-cpugraph-plugin
    # xfce.xfce4-dict
    # xfce.xfce4-fsguard-plugin
    # xfce.xfce4-genmon-plugin
    # xfce.xfce4-netload-plugin
    # xfce.xfce4-panel
    # xfce.xfce4-pulseaudio-plugin
    # xfce.xfce4-systemload-plugin
    # xfce.xfce4-weather-plugin
    # xfce.xfce4-whiskermenu-plugin
    # xfce.xfce4-xkb-plugin
    # xfce.xfdashboard

    ## WM's
    ## Bar
    polybar
    # eww
    # waybar # wayland
    ## Icons, themes and gtk setter
    # lxappearance
    # tela-circle-icon-theme
    # capitaine-cursors
    # Themes to test, don't commit
    # adw-gtk3
    ## Applications Laucnher
    rofi
    # fuzzel # wayland
    ## lockscreen
    betterlockscreen
    ## Hotkey setter
    sxhkd
    ## Notification
    dunst
    ## Screenshot
    scrot
    ## Compositor
    picom

    # Text Editor
    ## Terminal
    # vim
    vimPlugins.nvim-treesitter.withAllGrammars
    helix
    ## GUI
    # emacs29-gtk3
    # (emacs-unstable-pgtk.overrideAttrs {
    #   treeSitter = true;
    # })

    # GUI Applications
    ## Image stuff
    # gimp
    # inkscape
    feh # wallpaper
    # swww # wayland wallpaper
    imv
    ## 3d render
    blender-hip
    ## Video Stuff
    mpv
    ## Browser
    # qutebrowser
    brave
    # librewolf
    mullvad-browser
    tor-browser-bundle-bin
    ## Office
    # libreoffice
    ## PDF
    zathura
    sioyek
    typst
    typst-fmt
    pandoc
    # texlive.combined.scheme-full
    tectonic
    ## chatting
    # discord

    # Utilities
    ## Android
    ### File sharing
    localsend

    # Terminal stuff
    ## CMD I need
    wget
    curl
    git
    zip
    unzip
    file
    fzf
    tmux
    btop
    inxi # system monitoring
    bottom
    groff
    ripgrep
    fd
    coreutils
    hexyl
    eza # ls
    bat # cat
    bat-extras.batman # man
    dysk # df replacement
    du-dust # du replacement
    duf # lsblk and df replacement
    zoxide # cd
    delta # git diff viewer
    hyperfine
    tokei
    procs
    stow
    aria
    nmap
    ## TUi Programms
    moar
    lf
    ### LF perviewers
    chafa
    pistol
    lazygit
    ## Terminal to gui
    xdragon
    ## Shells
    dash

    # Other utils
    xorg.xsetroot
    xdotool
    xsel
    xclip
    wl-clipboard
    networkmanager
    caffeine-ng
    # wlogout # wayland
    # seatd # needed for hyprland
    ## Rss
    # newsflash
    ## Sound
    volumeicon
    pulsemixer
    pamixer
    pavucontrol

    # Music
    playerctl
    spotify
    ncspot
    # ncmpcpp
    # mopidy
    # mopidy-mpd

    # Terminal
    alacritty
    (st.overrideAttrs {
      src = pkgs.fetchFromGitea {
        domain = "codeberg.org";
        owner = "tilmanmixyz";
        repo = "st";
        rev = "41c5bfe1081c747654bd727eae377dcf14bf783c";
        sha256 = "sha256-/WE2+9T8e3tClJXecxx1EJE250PjAhopnjdE8VlSnik=";
      };
    })

    # Programming languages
    # rustup # Rust
    clang_16
    # gcc13
    gnumake
    gnupatch
    (fenix.stable.withComponents [
      "cargo"
      "rust-std"
      "rustc"
      "rustfmt"
    ])
    cargo-info
    cargo-cross
    cargo-watch
    cargo-expand
    bacon

    # Command Runner
    just

    # LSP Server
    rust-analyzer
    taplo
    gopls
    zls
    lua-language-server
    typst-lsp
    clang-tools_16
    # Nix
    alejandra
    nil

    # Emacs till home-manager
    # emacsPackages.vterm

    # Nix-direnv
    direnv
    nix-direnv

    # Hyprland
    foot
    fuzzel
    wlogout
    swaylock
    swww
    (
      waybar.overrideAttrs (oldAttrs: {
        mesonFlags = oldAttrs.mesonFlags ++ ["-Dexperimental=true"];
      })
    )
    swaynotificationcenter
    mako
    grim
    slurp
    wf-recorder
    swappy

    # Games
    # zeroad
  ];
  services = {
    # Xorg
    xserver = {
      enable = true;
      videoDrivers = ["nvidia"];
      displayManager = {
        lightdm = {
          enable = false;
          greeters.gtk = {
            enable = true;
          };
        };
        # gdm.enable = true;
        sddm = {
          enable = false;
          theme = "${(
            pkgs.fetchFromGitHub {
              owner = "MarianArlt";
              repo = "sddm-sugar-dark";
              rev = "ceb2c455663429be03ba62d9f898c571650ef7fe";
              sha256 = "0153z1kylbhc9d12nxy9vpn0spxgrhgy36wy37pk6ysq7akaqlvy";
            }
          )}";
        };
      };
      # desktopManager = {
      #   xfce.enable = true;
      # };
      windowManager = {
        # awesome.enable = true;
        bspwm.enable = true;
        dwm = {
          enable = true;
          package = pkgs.dwm.overrideAttrs {
            src = pkgs.fetchFromGitea {
              domain = "codeberg.org";
              owner = "tilmanmixyz";
              repo = "dwm";
              rev = "a121a621754b6d060229bbe0e50e1ad76d3d352a";
              sha256 = "sha256-CxtY82S4LV/5imBUkXS5jomT/FPNaHlEV6txFgxseRM=";
            };
          };
        };
      };
      # Keyboard
      layout = "de";
      xkbVariant = "nodeadkeys";
      xkbOptions = "caps:escape";
      # LibInput: touchpad support
      libinput.enable = true;
    };
    printing = {
      enable = true;
      drivers = with pkgs; [epson-escpr gutenprint];
    };
    pipewire = {
      enable = true;
      pulse.enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      jack.enable = true;
    };
    gnome = {
      gnome-keyring.enable = true;
    };
    # avahi.enable = true;
    gvfs.enable = true;
    logrotate.checkConfig = false;
    fwupd.enable = true;
    dbus = {
      packages = [
        pkgs.gnome.seahorse
      ];
    };
  };

  xdg = {
    portal = {
      enable = true;
    };
  };

  sound.enable = true;

  virtualisation = {
    libvirtd = {
      enable = true;
      qemu = {
        ovmf = {
          enable = true;
          packages = [pkgs.OVMFFull.fd];
        };
      };
    };
    # Container
    ## Podman
    podman = {
      enable = true;
    };
    ## containerd
    containerd = {
      enable = false;
    };
  };

  hardware = {
    cpu.intel.updateMicrocode = true;
  };

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
      timeout = 3;
    };
    supportedFilesystems = ["ntfs"];
    blacklistedKernelModules = ["nouveau"];
    extraModprobeConfig = "options kvm_intel";
    kernelModules = ["kvm-intel" "kvm-amd"];
    # kernelPackages = pkgs.linuxPackages_latest;
    # kernelPackages = pkgs.linuxPackages_latest_hardened;
    # kernelPackages = pkgs.linuxPackages_xanmod_latest;
    kernelPackages = pkgs.linuxPackages_lqx;
  };

  networking = {
    networkmanager.enable = false;
    nameservers = ["194.242.2.3#adblock.dns.mullvad.net" "194.242.2.2#dns.mullvad.net"];
    hostName = "styx";
    firewall = {
      enable = true;
      allowedTCPPorts = [53317];
      allowedUDPPorts = [53317];
    };
  };

  systemd.network = {
    enable = true;
    networks = {
      "10-lan" = {
        matchConfig.Name = "en*";
        DHCP = "ipv4";
        ipv6AcceptRAConfig = {
          DHCPv6Client = "always";
        };
        extraConfig = ''
          DNSOverTLS=yes
        '';
      };
    };
  };

  services.resolved = {
    enable = true;
    dnssec = "true";
    domains = ["~."];
    fallbackDns = ["9.9.9.9#dns.quad9.net" "149.112.112.112#dns.quad9.net" "1.1.1.1#one.one.one.one"];
    extraConfig = ''
      DNSOverTLS=yes
    '';
  };

  time.timeZone = "Europe/Berlin";

  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_TIME = "de_DE.UTF-8";
      LC_NUMERIC = "de_DE.UTF-8";
      LC_MONETARY = "de_DE.UTF-8";
      LC_PAPER = "de_DE.UTF-8";
      LC_MEASUREMENT = "de_DE.UTF-8";
    };
  };

  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
  };

  security = {
    rtkit.enable = true;
    polkit.enable = true;
    sudo = {
      enable = false;
      extraConfig = "Defaults insults";
    };
    doas.enable = true;
    apparmor.enable = true;
    pam = {
      services = {
        # gdm.enableGnomeKeyring = true;
        swaylock.text = lib.readFile "${pkgs.swaylock}/etc/pam.d/swaylock";
      };
    };
  };

  fonts = {
    fontDir.enable = true;
    enableDefaultPackages = true;
    packages = with pkgs; [
      (nerdfonts.override {
        fonts = [
          "CascadiaCode"
          "FiraMono"
          "JetBrainsMono"
          "GeistMono"
        ];
      })
      corefonts
      winePackages.fonts
      vistafonts
      # emacs-all-the-icons-fonts
      overpass
      lato
      jost
      inter
      dejavu_fonts
      noto-fonts
      public-sans
    ];
    fontconfig = {
      enable = true;
    };
  };

  system = {
    stateVersion = "24.05";
    autoUpgrade = {
      enable = false;
      allowReboot = false;
    };
  };

  systemd = {
    extraConfig = "DefaultTimeoutStopSec=20s";
    user.services.polkit-gnome-authentication-agent-1 = {
      description = "polkit-gnome-authentication-agent-1";
      wantedBy = [ "graphical-session.target" ];
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };
  };

  environment = {
    localBinInPath = true;
    homeBinInPath = true;
    sessionVariables = {
      VISUAL = "hx";
      EDITOR = "hx";
      PAGER = "bat";
      MANPAGER = "batman";
      PATH = "$PATH:/home/tilman/.local/share/nvim/mason/bin";
    };
  };

  # greetd
  services.greetd = {
    enable = true;
    restart = true;
    settings = {
      terminal.vt = 1;
      default_session = let
        hyprlandSession = pkgs.writeTextFile rec {
          name = "hyprland-session.desktop";
          destination = "/${name}";
          text = ''
            [Desktop Entry]
            Name=Hyprland
            Exec=${pkgs.hyprland}/bin/Hyprland
          '';
        };
        zshSession = pkgs.writeTextFile rec {
          name = "zsh-session.desktop";
          destination = "/${name}";
          text = ''
            [Desktop Entry]
            Name=Zsh
            Exec=${pkgs.zsh}/bin/zsh
          '';
        };
        bspwmmSession = pkgs.writeTextFile rec {
          name = "bspwm-session.desktop";
          destination = "/${name}";
          text = ''
            [Desktop Entry]
            Name=Bspwm
            Exec=${pkgs.bspwm}/bin/bspwm
          '';
        };

        session_dirs = builtins.concatStringsSep ":" (
          [hyprlandSession] ++ [zshSession] ++ [bspwmmSession]
        );
      in {
        command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time --remember -g 'Moin Tilman' -s ${session_dirs} --remember-user-session --asterisks --power-shutdown 'systemctl poweroff' --power-reboot 'systemctl reboot'";
        user = "tilman";
      };
    };
  };

  # Hyprland
  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
  };

  hardware = {
    # Opengl
    opengl = {
      enable = true;
      driSupport32Bit = true;
    };

    # Most wayland compositors need this
    nvidia.modesetting.enable = true;
  };

  environment.sessionVariables = {
    # If your cursor becomes invisible
    WLR_NO_HARDWARE_CURSORS = "1";
    # Hint electron apps to use wayland
    NIXOS_OZONE_WL = "1";
  };

  xdg.portal.extraPortals = [pkgs.xdg-desktop-portal-gtk pkgs.xdg-desktop-portal-gtk];

  # end Hyprland
}
