# { disks ? [ "/dev/<disk>" ], ... }: {
#   disko.devices = {
#     disk = {
#       vda = {
#         device = builtins.elemAt disks 0;
#         type = "disk";
#         content = {
#           type = "table";
#           format = "gpt";
#           partitions = [
#             {
#               name = "ESP";
#               start = "1MiB";
#               end = "100MiB";
#               bootable = true;
#               content = {
#                 type = "filesystem";
#                 format = "vfat";
#                 mountpoint = "/boot";
#               };
#             }
#             {
#               name = "root";
#               start = "100MiB";
#               end = "100%";
#               part-type = "primary";
#               bootable = true;
#               content = {
#                 type = "filesystem";
#                 format = "ext4";
#                 mountpoint = "/";
#               };
#             }
#           ];
#         };
#       };
#     };
#   };
# }
diskDevice: {
  disk.${diskDevice} = {
    device = diskDevice;
    type = "disk";
    content = {
      type = "table";
      format = "gpt";
      partitions = [
        {
          name = "ESP";
          start = "1MiB";
          end = "128MiB";
          bootable = true;
          part-type = "primary";
          content = {
            type = "filesystem";
            format = "vfat";
            mountpoint = "/boot";
          };
        }
        {
          name = "swap";
          start = "128MiB";
          end = "8292MiB";
          content = {
            type = "swap";
            randomEncryption = true;
          };
        }
        {
          name = "root";
          start = "8292MiB";
          end = "100%";
          part-type = "primary";
          content = {
            type = "filesystem";
            format = "ext4";
            mountpoint = "/";
          };
        }
      ];
    };
  };
}
