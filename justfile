list:
  just --list

boot:
  doas nixos-rebuild --flake .# boot

run:
  nix run '.#nixosConfigurations.default.config.system.build.vm'

setup:
  -mkdir ~/.config/nix
  -echo "experimental-features = nix-command flakes" >> ~/.config/nix/nix.conf

disks disk:
  doas nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode zap_create_mount ./hardware/disks.nix --arg disks '[ "/dev/{{disk}}" ]'
  doas nixos-generate-config --no-filesystems --root /mnt
  cp /mnt/etc/nixos/hardware-configuration.nix ./hardware/default.nix

install:
  git add .
  doas nixos-install --flake .

full disk:
  -just setup
  just disks {{disk}}
  just install

test:
  doas nixos-rebuild --flake . test

update:
  nix flake update

switch:
  doas nixos-rebuild --flake . switch
