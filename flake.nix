{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    helix = {
      url = "github:helix-editor/helix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = inputs @ {
    self,
    nixpkgs,
    fenix,
    disko,
    helix,
    emacs-overlay,
    home-manager,
    ...
  }: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
  in {
    nixosConfigurations = {
      styx = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [
          ({
            config,
            pkgs,
            lib,
            ...
          }: {
            nix.registry.nixpkgs.flake = nixpkgs;
          })
          {
            nixpkgs.overlays = [fenix.overlays.default helix.overlays.default emacs-overlay.overlays.default];
          }
          # disko.devices = import ./hardware/disks.nix "/dev/sdb"
          ./modules/networking/hosts.nix
          # ./modules/gitea/default.nix
          # ./modules/warden
          ./hosts/styx
          ./hardware
          # disko.nixosModules.disko
          # (import ./hardware/disks.nix {
          #   disks = ["/dev/vda"];
          # })
          home-manager.nixosModules.home-manager
          {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              extraSpecialArgs = {
                inherit inputs;
              };
              users.tilman = import ./home/users/tilman.nix;
            };
          }
        ];
      };
    };
    devShells.x86_64-linux.default = with nixpkgs.legacyPackages.${system};
      mkShell {packages = [just];};
    formatter.${system} = pkgs.alejandra;
  };
}
