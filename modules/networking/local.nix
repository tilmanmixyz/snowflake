{
  config,
  lib,
  pkgs,
  ...
}: {
  networking.extraHosts = ''
    192.168.178.40 milan-nas-1.fritz.box
  '';
}
