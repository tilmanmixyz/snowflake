{
  config,
  pkgs,
  ...
}: {
  home = {
    username = "tilman";
    homeDirectory = "/home/tilman";
    stateVersion = "24.05";
  };

  # Theming
  gtk = {
    enable = true;
    theme = {
      package = pkgs.adw-gtk3;
      name = "adw-gtk3-dark";
    };
    cursorTheme = {
      package = pkgs.capitaine-cursors;
      name = "Capitaine Cursors - White";
    };
    iconTheme = {
      # package = pkgs.tela-circle-icon-theme;
      package = pkgs.gruvbox-dark-icons-gtk;
      name = "oomox-gruvbox-dark";
    };
  };
  qt = {
    enable = true;
    platformTheme = "gtk";
    style = {
      name = "adwaita-dark";
      package = pkgs.adwaita-qt;
    };
  };

  programs.home-manager.enable = true;
}
