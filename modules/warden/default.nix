{
  config,
  lib,
  pkgs,
  ...
}: {
  services.vaultwarden = {
    enable = true;
    dbBackend = "postgresql";
  };

  services.caddy = {
    enable = true;
    virtualHosts = {
      "vault.styx.local" = {
        extraConfig = ''
          reverse_proxy localhost:8222
        '';
      };
    };
  };
}
