{
  modulesPath,
  specialArgs,
  options,
  config,
  pkgs,
  lib,
}: {
  services.gitea = {
    enable = true;
    package = pkgs.forgejo;
    appName = "NixOS Git";
    settings = let
      docutils = pkgs.python311.withPackages (ps:
        with ps; [
          docutils
          pygments
        ]);
    in {
      server = {
        DOMAIN = "git.styx.local";
        ROOT_URL = "https://git.styx.local/";
        HTTP_PORT = 8080;
      };
      "markup.restructurestext" = {
        ENABLE = true;
        FILE_EXTENSIONS = ".rst";
        RENDER_COMMAND = "${docutils}/bin/rst2html.py";
        IS_INPUT_FILE = false;
      };
    };
  };

  services.caddy = {
    enable = true;
    virtualHosts = {
      "git.styx.local" = {
        extraConfig = ''
          reverse_proxy localhost:8080
        '';
      };
    };
  };
}
