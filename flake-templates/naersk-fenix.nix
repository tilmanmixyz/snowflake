{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = {
    self,
    nixpkgs,
    flake-utils,
    fenix,
    naersk,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};
      rustToolchain = fenix.packages.${system}.fromToolchainFile {
        dir = ./.;
        sha256 = pkgs.lib.fakeSha256;
      };
      naersk-lib = naersk.lib.${system}.override {
        cargo = rustToolchain;
        rustc = rustToolchain;
      };
      nativeBuildInputs = with pkgs; [
        pkg-config
        rustToolchain
      ];
      buildInputs = with pkgs;
        [
        ]
        ++ pkgs.lib.optionals pkgs.stdenv.isDarwin [
          pkgs.libiconv
        ];
      bin = naersk-lib.buildPackage {
        src = ./.;
        doCheck = true;
        pname = "name";
        version = "0.1.0";
        inherit nativeBuildInputs buildInputs;
      };
    in {
      packages = {
        inherit bin;
        default = bin;
      };
      apps = {
        default = flake-utils.lib.mkApp {
          drv = bin;
        };
      };
      devShell = pkgs.mkShell {
        inherit buildInputs nativeBuildInputs;
        packages = with pkgs; [
          bacon
          tokei
        ];
      };
    });
}
